# Full Stack Hiring Project

This is to check your proficiency in Full Stack Developement. We define Full Stack as being proficient in frontend, api development, database handling and documentation. It is expected to have some knowldge of DevOps, atleast comfortable in managing your own deployment. Using modern javascript best practises, including unit tests with test runners and pushing your code in production earns you bonus points!
 

## Problem

  You're expected to build a  URL Shortner. It need to have a simple UI, APIs and API documentation for  URL shortening as a service. APIs must support short url generation and long url retrieval at a minimal level.  Javascript conventions and features should be used and best practise of development is expected to be followed. Any additional feature is a plus

## Submission

 Create a git repo on gitlab or github, whichever you are more comfortable with, and share the link of the code base.

## Questions

For any questions, please contact anivar.a@kinaracapital.com

  
## Bonus Points

  

1. **+ 5%** : If you have added proper unit test with test runner in the code. Test coverage should be ideally greater than 80%.

2. **+ 10%** : User Authentication and user level links management 

3. **+ 10%** : Additional API functions. 


If you have added proper production logging in the queueing system with file rotate and hosted it on a publically accessible ip, use free credits in AWS, google cloud or digitalOcean .. PLEASE NOTE: This does not remove the requriement of submitting the code base.
