# Flutter Hiring Project

This is to check your proficiency in Flutter/ Dart based developement. Ensure unit test with test runners and pushing your code in production earns you bonus points!
 

## Problem

  You're expected to build a Book search, list  and download app based on [Google BooksAPIs](https://developers.google.com/books/docs/v1/using). The application should be having various filters controlling the view.  Follow best practices. 

  
## Submission

 Create a git repo on gitlab or github, whichever you are more comfortable with, and share the link of the code base.

## Questions

For any questions, please contact anivar.a@kinaracapital.com

  
## Bonus Points

  

1. **+ 5%** : If you have added proper unit test with test runner in the code. Test coverage should be ideally greater than 80%.
2. **+ 5%** : If you have added proper comments with README for running the codeabse locally 

3. **+ 10%** : User Authentication via Google Login
